/*
 * TEST LOG with LSM9DS0_ahrs_mahony
 * 
 * 23/3/19:
 *   - Fixed the stability issue: the GUI data were output in dps instead of rps. 
 *   - Madgwick not as good as Mahony with LSM:  relative angles are wrong. 
 *   - Madgwick with NXP ok, not significantly better ith 5 msec than 30 msec period. Possibly even better with 50 msec?
 *   - Max frequency = 100Hz (even with Mahony).  
 *  
 *   - Checked the NXP rps vs. dps issue:
 *     - In library: conversion to degrees and then radians. Begin without argument defaults to 250DPS full range.
 *       Factor is correct GYRO_SENSITIVITY_250DPS=0.0078125F // Table 35 of datasheet value = 7.8125 mdps. OK.  
 *       Register Ctrl0 must be configured (FS0=1 et FS1=1) to have the 250DPS range.  
 *        NB: Setting the CTRL_REG3[FS_DOUBLE] increases the dynamic range for each CTRL_REG0[FS] selection by a
 *        factor of two, from ±250/500/1000/2000°/s to ±500/1000/2000/4000°/s. (not used)
 *       Sketch personal/baudhuina/CompareLSM-NXP-GYRO: proves the NXP sensitivity is not correct. Seems it is using
 *       the 2000DPS full range. 
 *       ERROR FOUND: as of March 2019, the last release of Adafruit_FXAS21002C library is 1.1.2 and is buggy. 
 *       Gyroscope sensitivity is NOT configured in the begin() method. The master version available from  
 *       https://github.com/adafruit/Adafruit_FXAS21002C fixes the issue.
 *       This fixes the issue. 
 *   - Clarified that calibration is performed with data in 0.1 µT/step resolution AND APPLIED on data in µT.     
 *     (see On-Board SW document for details). 
 *   
 *   - NXP: régulièrement, le gyro se fige. Un redémarrage de la carte suffit, mais que se passe-t-il ?
 *      
 *   
 *   TO BE TESTED: What is the minimum update frequency with an acceptable quality ? For NXP and LSM?
 *   
 *   Effect of update period
 *                      Mahony                Madgwick
 *   10 msec    NXP: 
 *              LSM:
 *   30 msec    NXP:
 *              LSM:
 *   50 msec    NXP:
 *              LSM:
 *   75 msec    NXP:
 *              LSM:
 *   100 msec   NXP:
 *              LSM:
 *   
 *   A EXPLORER:
 *   Note: To avoid gimbal lock you should read quaternions not Euler
  // angles, but Euler angles are used here since they are easier to
  // understand looking at the raw values. See the ble fusion sketch for
  // and example of working with quaternion data.
 
 */
