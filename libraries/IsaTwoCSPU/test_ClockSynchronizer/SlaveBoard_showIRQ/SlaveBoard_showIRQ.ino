/*
    test_ClockSynchronizer/SlaveBoardShowIRQ.ino

    Just switch on a led in the IRQ to validate it is indeed called. 
*/

#define DEBUG
#include "DebugCSPU.h"

#include "ClockSynchronizer.h"
#include "Wire.h"

constexpr byte SlaveAddress = 0xBB;
#define IRQ_LED 9
unsigned long ts;

void setup() {
  DINIT(115200);
  pinMode(IRQ_LED, OUTPUT);
  Wire.begin(SlaveAddress);

  // test LED
  digitalWrite(IRQ_LED, LOW);
  delay(3000);
  digitalWrite(IRQ_LED, HIGH);
  
  ClockSynchronizer_Slave::begin();
  Serial << "Slave registered ; waiting to receive sync..." << ENDL;
  Serial.flush();
  while ( !ClockSynchronizer_Slave::isSynchronized()) {
    delay(10);
  }
  Serial << "Synchronized!!" << ENDL;
}

void loop() {
  if (ClockSynchronizer_Slave::isSynchronized()) {
    Serial  << "Sync received" << ENDL;
  }

#ifdef DBG_IRQ
    ClockSynchronizer_Slave::syncReceived = false;
#endif
  delay(500);
  digitalWrite(IRQ_LED, HIGH); // turn off LED turned on in IRQ handler
}
