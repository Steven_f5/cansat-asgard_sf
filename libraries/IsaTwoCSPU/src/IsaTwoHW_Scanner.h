/*
 * IsaTwoHWScanner.h
 *
 *  Created on: 26 janv. 2018
 *
 */

#pragma once
#include "HardwareScanner.h"
#include "IsaTwoConfig.h"
#ifdef RF_ACTIVATE_API_MODE
#include "IsaTwoXbeeClient.h"
#endif

// Macros to ease use of API or transparent modes with XBee modules.
// See IsaTwoAcquisitionProcess for usage examples.
#ifdef RF_ACTIVATE_API_MODE
#  define GET_RF_STREAM(Isa2_HW_Scanner) Isa2_HW_Scanner.getRF_XBee();
#  define RF_OPEN_STRING(object) object->openStringMessage(IsaTwoRecordType::StatusMsg);
#  define RF_OPEN_STRING_PART(object) object->openStringMessage(IsaTwoRecordType::StringPart);
#  define RF_OPEN_CMD_REQUEST(object) object->openStringMessage(IsaTwoRecordType::CmdRequest);
#  define RF_CLOSE_STRING(object) object->closeStringMessage();
#  define RF_CLOSE_STRING_PART(object) object->closeStringMessage();
#  define RF_CLOSE_CMD_REQUEST(object) object->closeStringMessage();
#else
#  define GET_RF_STREAM(Isa2_HW_Scanner) Isa2_HW_Scanner.getRF_SerialObject();
#  define RF_OPEN_STRING(object)
#  define RF_OPEN_STRING_PART(object)
#  define RF_OPEN_CMD_REQUEST(object)
#  define RF_CLOSE_CMD_REQUEST(object)
#  define RF_CLOSE_STRING(object)
#  define RF_CLOSE_STRING_PART(object)
#endif

/** The class used to interact with the Hardware of IsaTwo's main µController.
 *  This class should not be operationally used for any other controller, since it
 *  performs configurations (e.g. input and output pins) which could conflict with
 *  the hardware allocation on the controller.
 *  It can nevertheless be used as standalone utility to detect the available
 *  hardware capabilities.
 */
class IsaTwoHW_Scanner: public HardwareScanner {
  public:
    typedef struct Flags {
      unsigned int SD_CardReaderAvailable:1;
      unsigned int BMP_SensorAvailable:1;
      unsigned int IMU_SensorAvailable:1;
      // NB: no flag for the sensors: we cannot possibly detect.
    } Flags;
    
    IsaTwoHW_Scanner(const byte unusedAnalogInPin=0);
    virtual ~IsaTwoHW_Scanner() {};
    virtual void IsaTwoInit();
    virtual void checkAllSPI_Devices();
    /** Print the complete diagnostic
     *  This method completes the diagnostic provided by the HardwareScanner
     *  @param stream The stream on which the diagnostic must be printed.
     */
    virtual void printFullDiagnostic(Stream& stream) const;
    virtual void printSPI_Diagnostic(Stream& stream) const;
    virtual void printI2C_Diagnostic(Stream& stream) const;
    virtual byte getLED_PinNbr(LED_Type type);
    const Flags& getFlags() const { return flags;};
    Stream* getGPS_SerialObject() const {
    	return getSerialObject(GPS_SerialPortNumber);
    }
#ifdef RF_ACTIVATE_API_MODE
    virtual IsaTwoXBeeClient *getRF_XBee() {
    	if (xbClientAvailable) {
    		return &xbClient;
    	}
    	else return nullptr;
    }
#endif
#ifdef IGNORE_EEPROMS  // This is just used to avoid consuming write cycles during tests.
    virtual byte getNumExternalEEPROM() const;
#endif

  private:
    Flags flags;  //  The flags to indicate the availability of the various devices. 
#ifdef RF_ACTIVATE_API_MODE
    bool	xbClientAvailable;	/**< True if the xbClient was succesfully initialized */
    IsaTwoXBeeClient xbClient;  /**< The interface to communicate with the XBee in API mode */
#endif
    
};
