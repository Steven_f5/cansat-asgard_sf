/*
   IMU_ClientPrecisionNXP.cpp
*/

#include "IMU_ClientPrecisionNXP.h"

IMU_ClientPrecisionNXP::IMU_ClientPrecisionNXP(byte nSamplesPerRead):
	IMU_Client(nSamplesPerRead), accelmag(0x8700A, 0x8700B), gyro(0x0021002C) {}

bool IMU_ClientPrecisionNXP::begin() {
  if (!accelmag.begin(ACCEL_RANGE_2G)) return false;
  if (!gyro.begin(GYRO_RANGE_250DPS)) return false;
  return true;
}

void IMU_ClientPrecisionNXP::readOneData(IsaTwoRecord& record) {

	sensors_event_t unusedEevent, mevent;

	/* Get a new sensor event */
	accelmag.getEvent(&unusedEevent, &mevent);
	gyro.getEvent(&unusedEevent);

	record.accelRaw[0] = accelmag.accel_raw.x;
	record.accelRaw[1] = accelmag.accel_raw.y;
	record.accelRaw[2] = accelmag.accel_raw.z;

	/* (mag data is in uTesla) */
	record.mag[0] = mevent.magnetic.x;
	record.mag[1] = mevent.magnetic.y;
	record.mag[2] = mevent.magnetic.z;

	record.gyroRaw[0] = gyro.raw.x;
	record.gyroRaw[1] = gyro.raw.y;
	record.gyroRaw[2] = gyro.raw.z;
}
