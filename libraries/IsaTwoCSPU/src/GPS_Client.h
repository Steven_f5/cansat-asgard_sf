#include <Adafruit_GPS.h>
#ifdef __AVR__
#include <SoftwareSerial.h>
#endif
#include "IsaTwoRecord.h"
/**
   @ingroup IsaTwoCSPU
   @brief This class aquires the following data from the gps: latitude, longitude, altitude, velocity (direction and angular speed).
   Latitude & longitude are expressed in the WGS_1984 refential. Altitude is relative to the mean sea level (geoid).
   The data sent by the gps at a frequency of 10HZ will be then stored in an IsaTwoRecord.
*/
class GPS_Client {
  public:
    /**
         @param  serialPort The serial port to which the GPS is physically connected.
    */
    /**The feather m0 not supporting a software serial, there has to be a clause that assigns a hardwareSerial to the class if the feather is used*/
#ifdef __AVR__
    GPS_Client(SoftwareSerial& serialPort);
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
    GPS_Client(HardwareSerial& serialPort);

#endif
    /**
      Calls this method once in the the setup to configure the GPS device.
    */
    void begin();

    /**
        @brief Acquire GPS data and store in provided record
        @param record The IsaTwo record in which fields xxxx,yyy,zzz, are filled with GPS readings
    */
    void readData(IsaTwoRecord &record);
    /**
       Different methods that can be used to obtain data from the gps
    */
  private:
    /**< The Adafruit gps driver */
    Adafruit_GPS myGps;
    float lastLongitudeValue;
    float lastLatitudeValue;
};
