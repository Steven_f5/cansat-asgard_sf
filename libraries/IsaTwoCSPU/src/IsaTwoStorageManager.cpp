/*
    IsaTwoStorageManager.cpp

*/

#include "IsaTwoStorageManager.h"
#include "IsaTwoConfig.h"
#include "IsaTwoAcquisitionProcess.h"
#include "StringStream.h"

IsaTwoStorageManager::IsaTwoStorageManager(unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec)
  : StorageManager(sizeof(IsaTwoRecord), theCampainDurationInSec, theMinStoragePeriodInMsec) {

}

void IsaTwoStorageManager::storeIsaTwoRecord(const IsaTwoRecord& data, const bool useEEPROM)
{
  DPRINTSLN(DBG_STORAGE, "IsaTwoStorageManager::storeOneRecord");
  String CSV_Version;
  CSV_Version.reserve(200);
  StringStream sstr(CSV_Version);
  data.printCSV(sstr);
  const byte * binaryData= (const byte*) &data;
  storeOneRecord( binaryData, CSV_Version, useEEPROM);
}

