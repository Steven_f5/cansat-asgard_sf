#include "GPY_Calculator.h"
#include "IsatisConfig.h"

double GPY_Calculator::Voc = GPY_DefaultVocValue;

#ifdef GPY_INCREASE_VOC 
double GPY_Calculator::VoutReference = 0.0;
#endif

byte GPY_Calculator::VoutReferenceCount = 0;

void GPY_Calculator::adjustVoc(double outputV) {
  if (outputV < Voc) {
    // Consider decreasing Voc
    double threshold= Voc - (GPY_MinSignificantDeltaV/2);
    if (outputV <= threshold) {
      Voc = threshold;
#ifdef GPY_INCREASE_VOC 
      VoutReference = Voc;
#endif
    }
#ifdef GPY_INCREASE_VOC 
    VoutReferenceCount = 0;
#endif
  }
#ifdef GPY_INCREASE_VOC    
  // This algorith, although recommanded by Sharp causes any constant
  // measurement to become 0 by increasing Voc. This makes no sense....
  else if (fabs(outputV - VoutReference) < GPY_MinSignificantDeltaV)
  {
    DPRINTS(DBG_GPY_VOC, "outputV close to ");
    DPRINTLN(DBG_GPY_VOC, VoutReference, numDecimalPositionsToUse);
    VoutReferenceCount++;
    if (VoutReferenceCount >= GPY_NumSamplesToIncreaseVoc) {
      Voc = outputV;
      // NB: Do not set Voc to VoutReference: it could be more
      //     than outputV, resulting in negative density.
      VoutReferenceCount = 0;
      VoutReference = 0.0;
      DPRINTS(DBG_GPY_VOC, "Voc set to ");
      DPRINTLN(DBG_GPY_VOC, Voc, numDecimalPositionsToUse);
    } // >= num. samples
  } // close
  else {
    DPRINTS(DBG_GPY_VOC, "outputV not close to ");
    DPRINTLN(DBG_GPY_VOC, VoutReference, numDecimalPositionsToUse);
    VoutReference = outputV;
    VoutReferenceCount = 0;
  }
#endif
}

double GPY_Calculator::computeDustDensity(double deltaV) {
  if (GPY_ForceLowDensityToZero && (deltaV < (GPY_MinSignificantDeltaV/2.0))) {
    deltaV = 0.0;
  }
  return deltaV * GPY_SensitivityInMicroGramPerV;
}

double GPY_Calculator::getDustDensity(double outputV, bool qualityOK) {
  double dustDensity;

  // Do not adapt Voc during first reading which can be wrong. 
  static bool firstReadingDone = false; 
  if (firstReadingDone) {                
    if (qualityOK) {
      adjustVoc(outputV);
    }
    dustDensity = computeDustDensity(outputV - Voc);
  }                                        
  else {                                    
    firstReadingDone = true;            
    dustDensity = computeDustDensity(outputV - Voc); 
  }                                         

  return dustDensity;
}

double GPY_Calculator::getAQI(double dustDensity) {
  double aqiL = 0;
  double aqiH = 0;
  double bpL = 0;
  double bpH = 0;
  double aqi = 0;
  if (dustDensity <0) dustDensity=0; // noise could result in negative densities. 
  //According to the correspondence between pm2.5 and aqi were calculated aqi
  if (dustDensity >= 0 && dustDensity <= 35) {
    aqiL = 0;
    aqiH = 50;
    bpL = 0;
    bpH = 35;
  } else if (dustDensity > 35 && dustDensity <= 75) {
    aqiL = 50;
    aqiH = 100;
    bpL = 35;
    bpH = 75;
  } else if (dustDensity > 75 && dustDensity <= 115) {
    aqiL = 100;
    aqiH = 150;
    bpL = 75;
    bpH = 115;
  } else if (dustDensity > 115 && dustDensity <= 150) {
    aqiL = 150;
    aqiH = 200;
    bpL = 115;
    bpH = 150;
  } else if (dustDensity > 150 && dustDensity <= 250) {
    aqiL = 200;
    aqiH = 300;
    bpL = 150;
    bpH = 250;
  } else if (dustDensity > 250 && dustDensity <= 350) {
    aqiL = 300;
    aqiH = 400;
    bpL = 250;
    bpH = 350;
  } else if (dustDensity > 350) {
    aqiL = 400;
    aqiH = 500;
    bpL = 350;
    bpH = 500;
  }
  //formula: aqi = (aqiH - aqiL) / (bpH - bpL) * (desity - bpL) + aqiL;
  aqi = (aqiH - aqiL) / (bpH - bpL) * (dustDensity - bpL) + aqiL;
  return aqi;
}
