/*
 * A subclass for testing the HardwareScanner
 */

#pragma once
#include "HardwareScanner.h"

class HW_Subclass: public HardwareScanner {
public:
	HW_Subclass();

	virtual byte getNumExternalEEPROM() const { return 3;}
	virtual byte getExternalEEPROM_Address(const byte EEPROM_Number) const ;
	virtual unsigned int getExternalEEPROM_LastAddress(const byte EEPROM_Number) const ;

	virtual void checkAllSPI_Devices();

	virtual byte getPinNbr_StorageLED();
	virtual byte getPinNbr_AcquisitionLED();
};
