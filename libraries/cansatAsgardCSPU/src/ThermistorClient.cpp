/*
   ThemistorClient.cpp
*/
#include "ThermistorClient.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG_READ  0
#define DBG_READ_AVG  0

#ifdef ARDUINO_AVR_UNO
constexpr uint16_t Nsteps = 1023;
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
constexpr uint16_t Nsteps = 4095;
#else
#error "Board not recognised"
#endif

static constexpr int NumAverageSamples = 50; // Number of samples of the voltage averaged at each measurement.

ThermistorClient::ThermistorClient(const float theVcc, const uint8_t theAnalogPinNbr, const float theRefResistor, const float theA, const float theB, const float theC, const float theD, const float theSerialResistor) {
  refResistor = theRefResistor;
  serialResistor = theSerialResistor;
  A = theA;
  B = theB;
  C = theC;
  D = theD;
  analogPinNbr = theAnalogPinNbr;
  Vcc = theVcc;
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
  analogReadResolution(12);
#endif
}

float ThermistorClient::readResistance()const {
  float V = 0.0;
  for (int i = 0; i < NumAverageSamples; i++)
  {
    int sensorValue = analogRead(analogPinNbr);
    V += sensorValue * (Vcc / Nsteps);
    DPRINTS(DBG_READ_AVG, "   SensorValue=");
    DPRINT(DBG_READ_AVG, sensorValue);
    DPRINTS(DBG_READ_AVG, ", V=");
    DPRINTLN(DBG_READ_AVG, V);
  }
  V /= NumAverageSamples;
  DPRINTS(DBG_READ, "numSamples=");
  DPRINT(DBG_READ, NumAverageSamples);
  DPRINTS(DBG_READ, ", V=");
  DPRINT(DBG_READ, V);
  DPRINTS(DBG_READ, ", serial resistor=");
  DPRINT(DBG_READ, serialResistor);
  DPRINTS(DBG_READ, ", Vcc=");
  DPRINT(DBG_READ, Vcc);
  DPRINTS(DBG_READ, ", Nsteps=");
  DPRINTLN(DBG_READ, Nsteps);
  return (V * serialResistor) / (Vcc - V);
}

float ThermistorClient::readTemperature()const {
  float R = readResistance();
  DPRINTS(DBG_READ, "refResistor=");
  DPRINT(DBG_READ, refResistor);
  DPRINTS(DBG_READ, " R=");
  DPRINTLN(DBG_READ, R);
  float logRRref = log(R / refResistor);
  float temp = 1 / ((((D * logRRref + C) * logRRref + B ) * logRRref) + A);
  return temp - 273.15;
}
