/*
   ThermistorClient.h
*/
#pragma once

#include "Arduino.h"
/**
 * @brief this class calculate the temperature
 */
class ThermistorClient {
  public:
  /**
   * @param theAnalogPinNbr this is the number of the analog pin
   * @param theRefResistor this is the resistor at 25 degrees
   * @param theA is the first parameter use to calculate the temperature
   * @param theB is the second parameter use to calculate the temperature
   * @param theC is the thrird parameter use to calculate the temperature
   * @param theD is the fourth parameter use to calculate the temperature
   * @param theSerialResistor this is the value of the resistor we have put in serie, here that is 10K ohms.
   */
    ThermistorClient(const float theVcc, const uint8_t theAnalogPinNbr, const float theRefResistor, const float theA, const float theB, const float theC, const float theD, const float theSerialResistor);
    float readTemperature()const;
  private:
  /**
   * @brief it read the voltage of the thermistor
   */
    float readVoltage()const;
    /**
     * @brief it read the resistance 
     */
    float readResistance()const;
    float A, B, C, D, Vcc;
    float analogPinNbr;
    float refResistor, serialResistor;
};
