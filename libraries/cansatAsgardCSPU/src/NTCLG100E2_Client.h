/*
   NTCLG100E2_Client.h
*/

#pragma  once
#include "ThermistorClient.h"

/**
   @brief This a subclass of thermistorClient customized for thermistor NTCLG100E2104JB, NTCSMELFE3203Jx  or  NTCLG100E2203Jx.
   Use this class to read thermistor resistance and convert to degrees.
   wiring VCC to thermistor, thermistor to serialresistor, serialresistor to ground.

*/
class NTCLG100E2_Client: public ThermistorClient {
  public:
  /**
      @param theAnalogPinNbr this is the pin of the card in which we put the cable to read the resistance.
      @param theSerialResistor The serial resistor (ohms).
    */
    NTCLG100E2_Client(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorClient(theVcc, theAnalogPinNbr, 10000.0, 0.003354016,  0.0002569850, 2.620131E-06, 6.383091E-08, theSerialResistor) {};
};
