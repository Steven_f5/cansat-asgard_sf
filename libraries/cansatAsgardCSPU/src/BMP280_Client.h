/*
    BMP280_Client.h
*/
#pragma once

#include "Adafruit_BMP280.h"
#include "Adafruit_Sensor.h"
#include "Wire.h"
/** @ingroup IsaTwoCSPU
    xxxxxxx
*/
class BMP280_Client {
  public:
    BMP280_Client();
    /**
        @brief Initialise the BMP280 sensor before use.
        @param theSeaLevelPressure The sea level pressure in hPa.
        @return True if initialisation was succesfull, false otherwise.
    */
    bool begin(float theSeaLevelPressure);

    //DOC 
    float getPressure();

    float getAltitude();

    float getTemperature() ;


  protected:
    Adafruit_BMP280 bmp;
    float seaLevelPressure; // in hPA

};
