/*
   XBeeClient.h

*/

#pragma once

#include "Arduino.h"
#include <XBee.h>
#include "StringStream.h"

class XBeeClient {
  public:
    /**Build up constructor - ATTENTION, values must be given in hexa (ex: 0x0013a200)
      @param SH in the Serial Number High specific to the receiving Xbee
      @param SL is the Serial Number Low specific to the receiving Xbee

    */
    XBeeClient(uint32_t SH, uint32_t SL);
    /** Initialize point to point radio link
        @param stream The serial object to use to communicate with
                the XBee module. */
    virtual void begin(HardwareSerial &stream);

    /** Send data.
        @param data The payload to transfer
        @param dataSize The number of bytes of the payload
        @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.
        @param SH The destination address (high word)
        @param SL The destination address (low word).  If SH=SL=0, the default
        			   address provided in the constructor is used.
        @return True if transfer successful, false otherwise.
    */
    virtual bool send(uint8_t* data,
                      uint8_t dataSize,
                      int timeOutCheckResponse = 300,
                      uint32_t SH = 0,
                      uint32_t SL = 0);
    /** Check whether data is received, the user should then print data using a for{} loop
        @param payload If anything is received, this pointer is set
                 to an internal buffer containing the received
                 payload.
        @param payloadSize If anything is received, this parameters is
                 set to the number of bytes of the received payload.
        @return True if anything received, false otherwise
    */
    virtual bool receive(uint8_t* &payload, uint8_t& payloadSize);

    virtual void openStringMessage(uint8_t type);
    virtual bool closeStringMessage();

  private:
    XBee xbee;
    XBeeAddress64 defaultReceiverAddress;
    String msgBuffer; /**<The internal buffer to build the string messages */
    StringStream sstr; /**< A stream to fill the buffer */
    template<class T> friend XBeeClient &operator <<(XBeeClient &xb, T arg);
};

template<class T> inline XBeeClient &operator <<(XBeeClient &xb, T arg) {
  xb.sstr << arg; return xb;
}
