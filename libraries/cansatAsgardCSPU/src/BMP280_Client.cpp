/*
   BMP280_Client.cpp
*/
#include "BMP280_Client.h"

BMP280_Client::BMP280_Client(): bmp() {}

bool BMP280_Client::begin(float theSeaLevelPressureInHPa) {
  if (!bmp.begin()) return false;
  seaLevelPressure = theSeaLevelPressureInHPa;
  return true;
}

float BMP280_Client::getPressure() {
  return  bmp.readPressure() / 100.0;
}

float BMP280_Client::getAltitude() {
  float altitude = bmp.readAltitude(seaLevelPressure);
  if (isnan(altitude)) {
    altitude = 0.0;
  }
  return altitude;
}

float BMP280_Client::getTemperature() {
  return bmp.readTemperature();
}
