/*
   NTCLE101E3103SB0_Client.h
*/

#pragma  once
#include "ThermistorClient.h"

/**
  @brief This a subclass of thermistorClient customized for thermistor VMA320.
  Use this class to read thermistor resistance and convert to degrees.
  wiring VCC to thermistor, thermistor to serialresistor, serialresistor to ground.
*/
class NTCLE101E3103SB0_Client: public ThermistorClient {
  public:
    /**
      @param theAnalogPinNbr The pin connected to the thermistor-to-resistor connexion.
      @param theSerialResistor The value of the resistor connected to the thermistor (ohms).
    */
    NTCLE101E3103SB0_Client(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorClient(theVcc, theAnalogPinNbr, 10000, 0.003354016,  0.0002569850, 2.620131E-06, 6.383091E-08, theSerialResistor) {};
};
