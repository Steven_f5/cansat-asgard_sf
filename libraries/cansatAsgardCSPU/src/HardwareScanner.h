/* 
 *  HardwareScanner.h
*/

#pragma once
// Double include guard: for unknown reason, in the Arduino environment,
// in some ill-defined circumstances, the pragma once directive seems to be ignored.
#ifndef __HARDWARESCANNER_H__
#define __HARDWARESCANNER_H__

#include "Arduino.h"
#include "VirtualMethods.h"

/** @ingroup cansatAsgardCSPU
 *  @brief A generic helper class to check whether required HW is indeed present
   and responsive and to provide access to it.

   Scans the I2C bus, and can be subclassed to check for SPI slaves (implement
   scanForSPI_Devices() method.
   The following assumptions are made. Use a subclass to work on different assuptions.
   - Addresses 0x50 to 0x57 are assumed to be I2C EEPROMs
   - All I2C EEPROMs are assumed to be the same size (provided to constructor).

   Size of the object: about 20 to 30 bytes (depends on the board used). 

   @remark Currently supports the AVR board and SAMD board. 
   - For SAMD board, the HardwareScanner
     includes "Serial2.h" which causes the sketch to configure Sercom1 for use as Serial2 port.
     Would the program use SerCom1 for any other purpose, this would create a conflict.  
   - For SAMD boards, the I2C delay seems to be much longer than on Uno: takes about 0.5 sec to explore
     and I2C address, which makes the scanner quite slow. On Uno board, it's much much faster... 
   
   Possible optimizations:
   - Store port number of RF_Serial, rather than pointer to Serial (save 1 byte). But how to find the
     serial object just from the number?
   - Pack I2C addresses?  7 bits used on the 8. Addresses 0 to 7 are reserved so 6 bits are really necessary.
     Use 128 1 bit flags, i.e. 16 bytes for all possible addresses? This increases the size but could
     be combined with a template definition allowing to have an class supporting 8n addresses in a predefined
     range, using n+1 byte (1 for the first address).
 */
class HardwareScanner
{
  public:
	/** The various roles allocated to board LEDs */
    typedef enum LED_Type  {
      Init,         /**< Switched on during initialization 					*/
      Storage,      /**< Switched on during storage 						*/
      Transmission, /**< Switched on during RF transmission 				*/
      Acquisition,  /**< Switched on during Acquisition of sensor data 		*/
      Heartbeat,    /**< Blinks continuously 								*/
      Campaign,     /**< Switched on when measurement campaign is started 	*/
      UsingEEPROM   /**< Can be used to denote the usage of EEPROM (for subclass usage). */
    } LED_Type;

    static const unsigned int maxNbrOfI2C_Slaves = 10; /**< The maximum number of I2C slaves managed.
    														Increasing increases the memory foot-print
    														of the HardwareScanner object */
    /** @param unusedAnalogInPin The number of an unused analog input pin. It is used
     *         to read a random seed.
     */
    HardwareScanner(const byte unusedAnalogInPin=0);

    VIRTUAL ~HardwareScanner() {};

    /** Call once, before using, once the setup() function of the sketch has been entered
       (to ensure all board functions are up and running)
       Note that this call takes care of calling Wire.begin(), and setting the clock rate to 400kHz
       @param firstI2C_Address  First I2C address to consider.
       @param lastI2C_Address   Last I2C address to consider.
       @param RF_SerialPort The ID of the serial port to use for the RF transmitter (0 = none,
       	   	   	   	   	    1 = Serial1, 2 = Serial2 etc.)
       @param bitRateInKhz  bitRate to use to initialize the Wire library.
    */
    VIRTUAL_FOR_TEST void init(  const byte firstI2C_Address = 1,
                        const byte lastI2C_Address = 127,
                        const byte RF_SerialPort = 0,
                        const uint16_t bitRateInKhz = (uint16_t) 400);

    /* ----------------------------------------------------------------------------------------- */
    /** @name 1. Diagnostic methods
     *  Those methods are used to print information about the hardware status.
     *  @{
     */
    /** Print the complete diagnostic
     *  @param stream The stream on which the diagnostic must be printed.
     */
    VIRTUAL void printFullDiagnostic(Stream& stream) const;

    /**  @brief Print the list of I2C slaves detected on the I2C bus
     *   If expanding the printI2C_Diagnostic, the subclass should call the inherited method before generating
     *   any additional diagnostic.
     *   @param stream The stream on which the diagnostic must be printed.
     */
    VIRTUAL void printI2C_Diagnostic(Stream& stream) const;


    /**  @brief Print the information about the available serial ports.
     *   @param stream The stream on which the diagnostic must be printed.
     */
    NON_VIRTUAL void printSerialPortsDiagnostic(Stream& stream) const;

    /** @brief Print information about the SPI bus.
     *  Implement this method in subclass if any SPI device is used (this version just prints a
     *  placeholder message. The subclass should also provide its own methods to check whether
     *  the SPI slaves are up and running.
     *  @param stream The stream on which the diagnostic must be printed.
     */
    virtual void printSPI_Diagnostic(Stream& stream) const;
    /** @} */

    // --------------------------------------------------------------------------------------
    /** @name 2. Runtime query methods
	 *  Those methods are used to dynamically check the status of particular pieces of hardware.
     *  First methods should usually not need any implementation in subclasses.
     *  @{
     */

    /** Check whether a particular I2C slave is up and running
     *  @param slaveAddress The I2C address of the slave to check.
     *  @return True if the slave is detected and responsive.
     */
    NON_VIRTUAL bool isI2C_SlaveUpAndRunning(const byte slaveAddress) const;

    /** Check whether a particular serial port is available.
     *  @param portNbr The number identifying the serial port (1=Serial, 2=Serial2 etc.)
     *  @return True if the port is available.
     */
    NON_VIRTUAL bool isSerialPortAvailable(const byte portNbr) const;

    /** Obtain a particular serial port object.
      *  @param portNbr The number identifying the serial port (1=Serial, 2=Serial2 etc.)
      *  @return A pointer to the object if the port is available, null otherwise
      */
    NON_VIRTUAL Stream* getSerialObject(byte portNbr) const;

    /** Obtain the number of external EEPROM chips detected on the I2C bus.
     *  @return The number of chips.
     */
    VIRTUAL byte getNumExternalEEPROM() const;

    /** Obtain a pointer to the Serial stream used to access the radio module.
     *  @return A pointer to the Serial stream, or NULL if none is available.
     */
    VIRTUAL HardwareSerial* getRF_SerialObject() const;

    /** Scan the I2C bus and update results for access through isI2C_SlaveUpAndRunning() and other
     *  query methods
     *  @param firstI2C_Address  First I2C address to consider.
     *  @param lastI2C_Address   Last I2C address to consider.
     *  @param bitRateInKhz  bitRate to use to initialize the Wire library.
     */
    NON_VIRTUAL void scanI2C_Bus( const byte firstAddress = 1, const byte lastAddress = 120,
                              const byte bitRateInKhz = (byte) 400);

    /** @brief Obtain the I2C address of a particular EEPROM chip.
     *  This method should never be called if getNumExternalEEPROM() returns 0.
     *  Practically, any system using external EEPROM should subclass HardwareScanner,
     *  Implement getNumExternalEEPROM(), getExternalEEPROM_Address() and getExternalEEPROM_Size().
     *  @param EEPROM_Number The EEPROM_Number to look for. Should be >=0 and < getNumExternalEEPROM().
     *  @warning This method relies on assuptions described in the header of this file.
     *  		 Overwrite them in subclass if these assumptions are not applicable to you.
     */
    VIRTUAL_FOR_TEST byte getExternalEEPROM_I2C_Address(const byte EEPROM_Number) const ;

    /** @brief Obtain the last address in a particular EEPROM chip.
     *  This method should never be called if getNumExternalEEPROM() returns 0.
     *  Practically, any system using external EEPROM should subclass HardwareScanner,
     *  Implement getNumExternalEEPROM(), getExternalEEPROM_Address() and getExternalEEPROM_Size().
     *  @param EEPROM_Number The EEPROM_Number to look for. Should be >=0 and < getNumExternalEEPROM().
     *  @warning This method relies on assuptions described in the header of this file.
     *  		 Overwrite them in subclass if these assumptions are not applicable to you.
     */
    VIRTUAL unsigned int getExternalEEPROM_LastAddress(const byte EEPROM_Number) const ;

    /** Obtain the pin number for a particular LED. This implementation always returns LED_BUILTIN.
     *  Redefine in subclass if you have more than one LED available. Return 0 if no LED is available
     *  for a particular function.
     *  @param type The type of the requested LED (as defined in HardwareScanner::LED_Type enum)
     *  @return The pinNumber (or 0 if no such LED).
     */
    VIRTUAL byte getLED_PinNbr(LED_Type type);

    /** Implement this method to check for your SPI devices one by one.
     *  This method is called as part of the init().
     */
    VIRTUAL void checkAllSPI_Devices() {};
    /** @} */ // End of group of Query methods.

    /** @brief Obtain the last address of the internal EEPROM if any.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  @return The last valid address in the EEPROM, 0 if no EEPROM available.
     */
    static int  getInternalEEPROM_LastAddress() {
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
      // This board has no EEPROM.
      return 0;
#else
      return E2END;
#endif
    };
    /** @} */  // End of group of query methods.

    /** @brief Obtain the default referenceVoltage for the board.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  @return The default reference voltage.
     */
    NON_VIRTUAL static float getDefaultReferenceVoltage() {
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
    	// This board works on 3.3V
    	return 3.3;
#else
    	// By default, assume Genuino Uno or similar
    	return 5.0;
#endif
    };

    /** @brief Obtain the number of steps of the board's analog/digital converters.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  This value can be used to define the voltage as V=ref*measure/numberOfSteps.
     *  return The number of steps (if ADC is n bits, return 2^n -1).
     */
    static int getNumADC_Steps() {
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
    	// This board has 12 bits ADC
    	return 4095;
#else
    	// By default, assume Genuino Uno or similar with 10 bits ADC
    	return 1023;
#endif
    };
  protected:
    /** Return true if the chip is 64 kbytes, false otherwise (it is then assumed to be 32 kBytes)
     *  @param I2C_Address The I2C address of the chip to test */
    bool check64Kbyte(const byte I2C_Address) const;

    byte I2C_Adresses[maxNbrOfI2C_Slaves];  /**< The internal structure holding the status of I2C slaves */
    byte nbrOfI2C_Slaves;					/**< The number of I2C slaves detected */
    unsigned int I2C_EEPROM_LastAddress;	/**< The I2C address of the last external EEPROM chip */
    byte I2C_EEPROM_Flags; 					/**< A table of the detected EEPROM chips.
    											 Each byte denotes EEPROM at address 0x50+bit order. */
    HardwareSerial *RF_Serial;				/**< The pointer to the RF Serial interface (or NULL if none) */
} ;
#endif
