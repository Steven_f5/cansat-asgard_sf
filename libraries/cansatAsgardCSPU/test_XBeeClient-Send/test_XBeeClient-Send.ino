/**
   Test for the sending part of XBeeClient.h
*/
#include "XBeeClient.h"
#define DEBUG
#include "DebugCSPU.h"
#define DBG 1

HardwareSerial &RF = Serial1;
XBeeClient xbc;
int counter = 0;
uint8_t payload[2] = {0};

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  digitalWrite(LED_BUILTIN, HIGH);
  DPRINTS(DBG, "Initialising Serials and communications...");
  RF.begin(115200);
  bool init = xbc.setSerial(RF);
  if (init) {
    DPRINTSLN(DBG, "OK!");
  } else {
    DPRINTSLN(DBG, "FAILED IN INIT");
  }
  DPRINTS(DBG, "---Beginning of the Tests---");
}

void loop() {
  for (int i = 0; i < sizeof(payload); i++) {
    payload[i] = counter++;
    if (counter >= 200) {
      counter = 0;
    }
  }
  bool result =  xbc.send(payload, sizeof(payload), 500, 0x0013a200, 0x41827f67);
  if (result) {
    DPRINTSLN(DBG, "Yeay it's working!");
  } else {
    DPRINTSLN(DBG, "***ERROR*** Ouille activate some debug overthere");
  }
}
