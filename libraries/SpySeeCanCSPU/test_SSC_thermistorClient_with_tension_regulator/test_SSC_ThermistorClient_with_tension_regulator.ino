/*
   test_SSC_ThermistorClient_wiyh_tension_regulator
*/

#define DEBUG
#include "DebugCSPU.h"
#include "SSC_ThermistorClient.h"
#include "SSC_BMP280_Client.h"
#include "NTCLE101E3103SB0_Client.h"

constexpr bool csvFormat = true;
constexpr bool readDuration = false;
constexpr float RefVoltage = 3.34;
constexpr float RsNTCLE = 32470.0;
constexpr float RsNTCLG = 177700;
constexpr float RsVMA = 9990.0;
constexpr float RsNTCLE101 = 177100.0;

NTCLE101E3103SB0_Client NTCLE101(RefVoltage, A3, RsNTCLE101);
SSC_ThermistorClient client(RefVoltage, A0, A1, A2, RsNTCLE, RsNTCLG, RsVMA) ;
SSC_BMP280_Client bmp;
SSC_Record record;


void setup() {
  // put your setup code here, to run once:
  DINIT(115200);

  analogReference (AR_EXTERNAL);
  if (csvFormat) {
    Serial << "time (seconds), BMP280, NTCLE100E3, NTCLG100E2, VMA320, NTCLE101" << ENDL;
  }
  bmp.begin(1037.6);
  Serial << "set up OK" << ENDL;
}

void loop() {
  record.clear();
  uint16_t startTS = millis();
  bool result = client.readData(record);
  uint16_t stopTS = millis();
  float T4 = NTCLE101.readTemperature();
  if (!result) {
    Serial << "*** error while reading temperature of the thermistors" << ENDL;
  }
  result = bmp.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature of the bmp" << ENDL;
  }

  if (csvFormat) {
    Serial << millis() / 1000.0 << ", " << record.temperatureBMP << ", " << record.temperatureThermistor1 << ", "
           << record.temperatureThermistor2 << ", " << record.temperatureThermistor3 << ", " << T4 << ENDL;
    if (readDuration) {
      Serial << ", read duration: " <<  stopTS - startTS << "msec" << ENDL;
    }
  }
  else
    record.print(Serial);

  delay(1000);

}
