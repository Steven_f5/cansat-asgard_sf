#include "Arduino.h"
#include "SSC_Record.h"

#define DEBUG
#include <DebugCSPU.h>
#define DBG 1

SSC_Record::SSC_Record () {
  SSC_Record::clear();
}

void SSC_Record::clear() {
  timestamp = 0;

  //A. GPS DATA
  newGPS_Measures = false;
  GPS_LatitudeDegrees = 0.00;
  GPS_LongitudeDegrees = 0.00;
  GPS_Altitude = 0.00;
#ifdef INCLUDE_GPS_VELOCITY
  GPS_VelocityKnots = 0.00;
  GPS_VelocityAngleDegrees = 0.00;
#endif

  //B. Primary Mission

  temperatureBMP = 0.00;
  pressure = 0.00;
  altitude = 0.00;
  temperatureThermistor1 = 0.00;
  temperatureThermistor2 = 0.00;
  temperatureThermistor3 = 0.00;

  //D. Secondary Mission

  startFrequency = 0;
  frequencyStep = 0;

  for (int index = 0; index < RF_POWER_TABLE_SIZE; index++) {
    setRF_PowerToNoData(index);
  }

}



void SSC_Record::printCSV(Stream& str, const float &f, bool finalSeparator, byte decimalsHere) const {
  str.print(f, decimalsHere);
  if (finalSeparator) {
    str << separator;
  }
}

void SSC_Record::printCSV(Stream& str, const bool b, bool finalSeparator) const {
  str.print((int) b);
  if (finalSeparator) {
    str << separator;
  }
}


void SSC_Record::printCSV_Part(Stream& str, DataSelector select, bool finalSeparator, HeaderOrContent headerOrContent) const {
  switch (headerOrContent) {
    case HeaderOrContent::Content:
      switch (select) {
        case DataSelector::GPS :
          printCSV(str, newGPS_Measures, true);
          printCSV(str, GPS_LatitudeDegrees, true);
          printCSV(str, GPS_LongitudeDegrees, true);
#ifdef INCLUDE_GPS_VELOCITY
          printCSV(str, GPS_Altitude, true);
          printCSV(str, GPS_VelocityKnots, true);
          printCSV(str, GPS_VelocityAngleDegrees, false);
#else
          printCSV(str, GPS_Altitude, false);
#endif
          break;

        case DataSelector::Primary :
          printCSV(str, temperatureBMP, true);
          printCSV(str, pressure, true);
          printCSV(str, altitude, true);
          printCSV(str, temperatureThermistor1, true);
          printCSV(str, temperatureThermistor2, true);
          printCSV(str, temperatureThermistor3, false);
          break;

        case DataSelector::Secondary :
          {
            printCSV(str, getStartFrequency(), true, 1);
            printCSV(str, getFrequencyStep(), true, 1);
            int index = 0;
            while ((index < RF_POWER_TABLE_SIZE - 1) && (hasRF_PowerData(index))) {
              printCSV(str, getRF_Power(index), true, 1);
              index++;
            }

            printCSV(str, getRF_Power(index), false, 1);
            break;
          }

        default :
          DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
          DASSERT (false);
          break;
      }
      if (finalSeparator) {
        str << separator;
      }
      break;


    case HeaderOrContent::Header:
      switch (select) {
        case DataSelector::GPS :
          str << F("GPS_Measures,GPS_LatitudeDegrees,");
          str << F("GPS_LongitudeDegrees,GPS_Altitude");
#ifdef INCLUDE_GPS_VELOCITY
          str << F(",GPS_VelocityKnots,GPS_VelocityAngleDegrees");
#endif
          break;

        case DataSelector::Primary :
          str << F("temperatureBMP,pressure,altitude,temperatureThermistor1,temperatureThermistor2,temperatureThermistor3");
          break;

        case DataSelector::Secondary :
          str << F("startFrequency,frequencyStep,");
          for (int index = 0; index < RF_POWER_TABLE_SIZE; index++) {
            str << F("RF_Power[") << index << F("]");
            if (index != RF_POWER_TABLE_SIZE - 1) {
              str << F(",");
            }
          }
          break;

        default:
          DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
          DASSERT (false);
          break;
      }
      if (finalSeparator) {
        str << separator;
      }
      break;

    default:
      DPRINT(DBG, "Unexpected HeaderOrContent value. Terminating Program.");
      DASSERT(false);
      break;
  }
}

void SSC_Record::printCSV(Stream& str, DataSelector select, HeaderOrContent headerOrContent) const {
  switch (headerOrContent) {
    case HeaderOrContent::Content:
      str <<  timestamp << separator; break;
    case HeaderOrContent::Header:
      str << F( "timestamp,"); break;
    default:
      DPRINT(DBG, "Unexpected HeaderOrContent value. Terminating Program.");
      DASSERT(false);
      break;
  }

  if (select == DataSelector::All) {
    printCSV_Part(str, DataSelector::GPS, true, headerOrContent);
    printCSV_Part(str, DataSelector::Primary, true, headerOrContent);
    printCSV_Part(str, DataSelector::Secondary, false, headerOrContent);
  }
  else if (select == DataSelector::PrimarySecondary) {
    printCSV_Part(str, DataSelector::Primary, true, headerOrContent);
    printCSV_Part(str, DataSelector::Secondary, false, headerOrContent);
  }
  else if (select == DataSelector::AllExceptSecondary) {
    printCSV_Part(str, DataSelector::GPS, true, headerOrContent);
    printCSV_Part(str, DataSelector::Primary, false, headerOrContent);
  }
  else {
    printCSV_Part(str, select, false, headerOrContent);
  }
}
