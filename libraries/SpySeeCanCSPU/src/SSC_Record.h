#pragma once

#include "Arduino.h"
#include "SSC_Config.h"

#undef INCLUDE_GPS_VELOCITY   // Define to transport GPS velocity data.

/** @ingroup SpySeeCanCSPU
    @brief The record carrying all data acquired or computed by the CanSat.
*/

class SSC_Record  {
  public:

    SSC_Record ();

    /** Enum values, allowing for restricting the CSV output to a subset of the record */
    enum class DataSelector {
      All,         /**< Select complete record */
      GPS,    /**< Select data from the GPS modules only */
      Primary, /**< Select data from primary mission only */
      Secondary,/**< Select data from secondary mission only */
      PrimarySecondary, /**< Select data from primary and secondary sensors only */
      AllExceptSecondary /**< Select complete record except the secondary mission */
    } ;

    /** Enum values, used to select what is printed: Header or content */
    enum class HeaderOrContent {
      Header, /**< Select CSV header*/
      Content /**< Select CSV content*/
    } ;

    /** Stream the record in human-readable format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                   complete records, if the data must be parsed by code expecting a SSC_Record).
    */
    void print(Stream& str, DataSelector select = DataSelector::All) const;

    /** Stream the record/header in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a SSC_Record).
        @param headerOrContent HeaderOrContent::Content if content should be printed, HeaderOrContent::Header if header should be printed.
        @warning  If the CSV output gets changed, SSC_Record::MaxCSV_Size and SSC_Record::MaxCSV_HeaderSize should imperatively be changed to 
                  (a little bit bigger than) the length of, respectively, the CSV content, and the CSV header.
    */
    void printCSV(Stream& str, DataSelector select = DataSelector::All, HeaderOrContent headerOrContent = HeaderOrContent::Content) const;

    /** Set all values to 0 or false */
    void clear();

    // Data members
    unsigned long timestamp{};  /**< Record timestamp in msec. */

    // A. GPS data
    bool  newGPS_Measures;        /**< true if GPS data is included in the record */
    float GPS_LatitudeDegrees;    /**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
    float GPS_LongitudeDegrees;   /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
    float GPS_Altitude;           /**< Altitude of antenna, in meters above mean sea level (geoid) */
#ifdef INCLUDE_GPS_VELOCITY
    float GPS_VelocityKnots;      /**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
    float GPS_VelocityAngleDegrees; /**< Direction of velocity in decimal degrees, 0 = North */
#endif

    // B. Primary mission data
    float temperatureBMP;    /**< The temperature in °C gathered from the BMP */
    float pressure;       /**< The pressure in hPA */
    float altitude;       /**< The altitude (m) as derived from the pressure */
    float temperatureThermistor1;    /**< The temperature in °C gathered from the first Thermistor*/
    float temperatureThermistor2;    /**< The temperature in °C gathered from the second Thermistor*/
    float temperatureThermistor3;    /**< The temperature in °C gathered from the third Thermistor*/

    // C. Secondary mission data
    static const byte RF_POWER_TABLE_SIZE = 45; // with the current record, max value is 45
    static constexpr float NO_POWER_DATA = -100.0f;
    static const uint16_t MaxCSV_Size=400;
    static const uint16_t MaxCSV_HeaderSize=800;

    /** Obtain the first frequency scanned for RF activity, in MHz
        @return the frequency in MHz
    */
    float getStartFrequency() const {
      return 433.0f + startFrequency / 10.0 ;
    };

    /** Set the first frequency scanned for RF activity
        @param the frequency in MHz
    */
    void setStartFrequency(float frequency) {
      startFrequency = (uint16_t) ((frequency - 433.0) * 10);
    };

    /** Get the frequency increase between two successive frequencies scanned for RF activity
        @return the step, in MHz
    */
    float getFrequencyStep() const {
      return frequencyStep / 10.0;
    };

    /** Set the frequency increase between two successive frequencies scanned for RF activity
        @param step the step, in MHz
    */
    void setFrequencyStep(float step) {
      frequencyStep = (uint8_t)(step * 10.0);
    };

    /** Get the power obtained in the indexth measurement
        @param index The index of the measurement (must be between 0 and RF_POWER_TABLE_SIZE-1)
        @return The power in DBm. -100 if no data is available.
    */
    float getRF_Power(uint8_t index) const {
      DASSERT(index>=0); DASSERT(index<RF_POWER_TABLE_SIZE);
      if (!hasRF_PowerData(index)) return NO_POWER_DATA;
      else return ((float)RF_Power[index]) / 10.0 - 30.0;
    };

    /** Set the power obtained in the indexth measurement
        @param index The index of the measurement (must be between 0 and RF_POWER_TABLE_SIZE-1)
        @param power  The power in DBm.
    */
    void setRF_Power(uint8_t index, float power) {
      DASSERT(index>=0); DASSERT(index<RF_POWER_TABLE_SIZE);
      uint8_t p  = (uint8_t) (round((power + 30.0) * 10.0));
      RF_Power[index] = (p >= 1 ? p : 1); // Value 0 is reserved for "no data"
    };

    /** Set the power obtained in the indexth measurement to the conventional value "No data"
        @param index The index of the measurement (must be between 0 and RF_POWER_TABLE_SIZE-1)
    */
    void setRF_PowerToNoData(uint8_t index) {
      DASSERT(index>=0); DASSERT(index<RF_POWER_TABLE_SIZE);
      RF_Power[index] = 0;
    };

    /** Test whether the indexth measurement has power data
        @param index The index of the measurement (must be between 0 and RF_POWER_TABLE_SIZE-1)
    */
    bool hasRF_PowerData(uint8_t index) const {
      DASSERT(index>=0); DASSERT(index<RF_POWER_TABLE_SIZE);
      return (RF_Power[index] != 0);
    };


  protected:
    /** Utility function: print a float, using the right number of decimal positions, from
        SSC_Config.h, with a final comma, if finalSeparator is true.
    */
    void printCSV(Stream& str, const float &f, bool finalSeparator = false, byte decimalsHere = numDecimalPositionsToUse) const;

    /** Utility function: print a bool as 1 or 0, with a final comma, if finalSeparator is true.
    */
    void printCSV(Stream& str, const bool b, bool finalSeparator = false) const;

    void printCSV_Part(Stream & str, DataSelector select, bool finalSeparator, HeaderOrContent headerOrContent) const;
    void print_Part(Stream & str, DataSelector select) const;
  private:
    static constexpr char separator = ',';
    uint16_t startFrequency; /**< in tenths of MHz counted from 433MHz */
    uint8_t frequencyStep;  /**< in tenths of MHz.  */
    uint8_t RF_Power[RF_POWER_TABLE_SIZE]; /**< Power in tenths of DBm from -30, 0 is reserved for no data.  */
    friend class SSC_Record_Test;
} ;
