/**
 *  SSC_SI4432_Client.h
 */

#pragma once
#include "SI4432_Client.h"
#include "SSC_Config.h"
#include "SSC_SI4432_Ant1_Client.h"
// #include "SSC_SI4432_Ant2_Client.h"
// #include "SSC_SI4432_Ant3_Client.h"

/**
 * @ingroup SpySeeCanCSPU
 * @brief This class takes care of the configuration of the SI4432 module. It can provide the RSSI (Received Signal Strength Intensity)on one or multiples frequencies (in dBm).
 * It is an instance for the SpySeeCan project.
 */

class SSC_SI4432_Client
{
public:
    /**
       @brief Initialise the the 3 antennas.
    */
    SSC_SI4432_Client();
    bool begin();
    /** @brief Read sensor data and populate data record.
        @param record The data record to populate (fields ...).
        @return True if reading was successful.
    */
    bool readData(SSC_Record &record);

private:
    SSC_SI4432_Ant1_Client ant1;
    powerDBm_t power[SSC_Record::RF_POWER_TABLE_SIZE];
};
