#include "SSC_SI4432_Client.h"

SSC_SI4432_Client::SSC_SI4432_Client() : ant1((uint8_t)SI4432_ChipSelect[0]) {}

bool SSC_SI4432_Client::begin()
{
    ant1.begin();
}

bool SSC_SI4432_Client::readData(SSC_Record &record)
{
    ant1.readPower(power, SSC_Record::RF_POWER_TABLE_SIZE, 0, 433.0, 868.0, 2.0);
    for (unsigned int i = 0; i < SSC_Record::RF_POWER_TABLE_SIZE; i++)
    {
        record.setRF_Power(i, power[i]);
    }
    record.setStartFrequency(433.0);
    record.setFrequencyStep(2.0);
    return true;
}