/**
 * This test is used for the SI4432 module. It calculates the attenuation and give it in c++ format 
 * to insert it in the SI4432_Client.
*/

#undef USE_RH_RF69
#define CSV_FORMAT
#define DEBUG
#include "DebugCSPU.h"
#include "SI4432_Client.h"

SI4432_Client si4432(5, 10);
#ifdef USE_RH_RF69
#include <RH_RF69.h>
RH_RF69 emitter(6, 11);
#else
RH_RF22 emitter(6, 11);
#endif
//Set to true if you want to :
constexpr bool CHANGE_EMITTER_FREQ = 0; //  the emitter to emit in every frequencies between 'minFreqMHz' and 'maxFreqMHz'
constexpr bool PRINT_POWER = 1;         //   to print the power after the test
constexpr bool PRINT_ATTENUATION = 0;   //  to print the attenuation after the test
constexpr bool EMIT = 0;                //  the emitter to do nothing and thus measure the noise
//constexpr bool ATTENUATION_CALCULATION2 = 1;  // to calculate the attenuation with the second method

unsigned long timestamp = millis();
constexpr unsigned long scanPeriod = 500;

constexpr float minFreqMHz = 433.0; // Configure freely this value
constexpr float maxFreqMHz = 868.0; // Configure freely this value (must be larger than minAttFrequency)
constexpr byte numStepFreq = 88;    // Configure freely this value (must be > 0 and <= 255)
constexpr byte attenuationSize = numStepFreq + 1;

#if (CHANGE_EMITTER_FREQ)
constexpr float emitFreqMHz = minFreqMHz;
#else
constexpr float emitFreqMHz = 750; // Configure this value if 'CHANGE_EMITTER_FREQ' is set to false to choose the frequency to emit on
#endif

constexpr float stepFreqMHz = (maxFreqMHz - minFreqMHz) / numStepFreq;
powerDBm_t power[attenuationSize];
//powerDBm_t powerAttCal[attenuationSize];
float attenuation[attenuationSize];

/*void attenuationCalculation(float table[])
{
  si4432.readPower(powerAttCal, attenuationSize, false, minFreqMHz, maxFreqMHz, stepFreqMHz);
  for (unsigned int i = 0; i < 0; i++)
  {
    powerAttCal = exp
  }
}*/

void setup()
{
  ///scanner
  si4432.begin();
  //manual reset
#ifdef USE_RH_RF69
  pinMode(12, OUTPUT);
  digitalWrite(12, LOW);
  delay(10);
  digitalWrite(12, HIGH);
  delay(10);
  digitalWrite(12, LOW);
  delay(10);
#endif

  /// emitter
  Serial.println("START");
  Serial.flush();
  if (!emitter.init())
  {
    Serial.println("emitter init failed");
    Serial.flush();
    while (!emitter.init())
    {
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
  else
  {
    Serial.println("Init successed");
  } // Defaults after init are 434.0MHz

#ifndef USE_RH_RF69
  emitter.setModemConfig(RH_RF22::UnmodulatedCarrier);
#endif
  emitter.setModeIdle();
  Serial.print("Mode changed for : ");
  Serial.println(emitter.mode());
  emitter.setFrequency(emitFreqMHz);
  if (!emitter.setFrequency(emitFreqMHz)) ///Checks if the emitter is initialized
  {
    Serial.println("*** Frequency not changed! ***");
    delay(1000);
    while (!emitter.setFrequency(emitFreqMHz))
    {
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }

  //Parameters
  Serial.println();
  Serial.println();
  Serial << "Minimum frequency : " << minFreqMHz << ", "
         << "Maximum frequency : " << maxFreqMHz << ", "
         << "Step frequency : " << stepFreqMHz << ", ";
  if (CHANGE_EMITTER_FREQ)
  {
    Serial << "Emitter emitting at diffrent frequencies between " << emitFreqMHz << " and " << maxFreqMHz << " MHz." ENDL;
  }
  else
  {
    Serial << "Emitter always emitting at " << emitFreqMHz << " MHz." ENDL;
  }
  if (!EMIT)
  {
    Serial.println("!!! Not emitting !!!");
  }

  //Set the power of the emitter
#ifdef USE_RH_RF69
  emitter.setTxPower(1, true);
#else
  emitter.setTxPower(RH_RF22_TXPOW_1DBM);
#endif
  delay(1500);

  /// ***TEST***

  unsigned int j = 0;
  float freq = minFreqMHz;
  emitter.setFrequency(emitFreqMHz);
  //si4432.readPower(powerAttCal, attenuationSize, false, minFreqMHz, maxFreqMHz, stepFreqMHz);
  while (freq <= maxFreqMHz)
  {
    if (CHANGE_EMITTER_FREQ)
    {
      emitter.setFrequency(freq);
    }
    delay(scanPeriod);
    if (EMIT)
    {
      emitter.setModeTx();
    }
    delay(50);
    power[j] = si4432.readPower(freq);
    emitter.setModeIdle();
    /*
      if(ATTENUATION_CALCULATION2){
        attenuation[j] = powerAttCal[j] - power;
      }*/
    attenuation[j] = -(power[j] - 1) / 2;
#ifdef CSV_FORMAT
    if (PRINT_POWER)
    {
      Serial << freq << ", " << power[j] << ", " << ENDL;
    }
    if (PRINT_ATTENUATION)
    {
      Serial << freq << ", " << attenuation[j] << ", " << ENDL;
    }
#else
    if (PRINT_POWER)
    {
      Serial.print(power);
    }
    if (PRINT_ATTENUATION)
    {
      Serial << "  [" << attenuation[j] << "]";
    }
    Serial << "  | ";
#endif
    freq += stepFreqMHz;
    j++;
  }
#ifdef CSV_FORMAT
  Serial << ENDL << ENDL
         << "constexpr float minAttFrequency = " << minFreqMHz << ";" << ENDL
         << "constexpr float maxAttFrequency = " << maxFreqMHz << ";" << ENDL
         << "constexpr byte numStepFreq = " << numStepFreq << ";" << ENDL
         << "constexpr byte attenuationSize = numStepFreq + 1;" << ENDL << ENDL
         << "constexpr float attStepFrequency = (maxAttFrequency - minAttFrequency) / numStepFreq;" << ENDL
         << "constexpr byte attenuationOffset[attenuationSize] = {" << ENDL
         << "  ";
  for (unsigned int i = 0; i < j; i++)
  {
    Serial << (int)attenuation[i];
    if (i != (j - 1))
      Serial << ", ";
    if (!((i + 1) % 5))
      Serial << ENDL << "  ";
  }
  Serial << ENDL
         << "};"
         << ENDL << ENDL;
#endif
  Serial.println();
  delay(1000);
}

void loop()
{
}
