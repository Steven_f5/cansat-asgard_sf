/*
 * SSC_HardwareScanner
 * 
 * This utility just creates an SSC_HardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#include "SSC_HW_Scanner.h"  

 void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.println("Running SpySeeCan Hardware Scanner...");
  Serial.println("Note: be sure to have pull-ups on SCL-SDA, otherwise detection is not reliable on Feather M0 Express");
  Serial.flush();
  SSC_HW_Scanner hw;
  hw.SSC_Init();
  hw.printFullDiagnostic(Serial);
}

void loop() {
  digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
  delay(500);
}
