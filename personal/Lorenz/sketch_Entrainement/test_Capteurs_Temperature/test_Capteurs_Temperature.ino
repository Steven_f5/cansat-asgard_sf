const int CapteurTemperature = A1;
const int LampeTemoinTemperature = 13;
const int CapteurPression = A3;
const int LampeTemoinPression = 8;
const int EcritureTemperature = 1000;
const int EcriturePression = 1000;
const int addr = 0;

#define DEBUG
#define DBG_TEMPERATURE 0

#include "EEPROM.h" 

void setup() {

  Serial.begin(9600);
  pinMode(CapteurTemperature, INPUT);
  pinMode(LampeTemoinTemperature, OUTPUT);
  pinMode(CapteurPression, INPUT);
  pinMode(LampeTemoinPression, OUTPUT);
  digitalWrite(LampeTemoinTemperature, HIGH);
  
  int valTemperature = analogRead(CapteurTemperature);
  float tensionTemperature = (valTemperature * 5.00) / 1023.0;
  float temperature = tensionTemperature * 100.00;
  Serial.println(temperature);
  delay(EcritureTemperature);

  digitalWrite(LampeTemoinPression, HIGH);
  int valPression = analogRead(CapteurPression);
  float tensionPression = valPression;
  float pression = tensionPression;
  Serial.println(pression);
  delay(EcriturePression);

  Serial.print("Fin de l'EEPROM:");
  Serial.println(E2END);
 EEPROM.write(addr, "coucou");


}

void loop() {

}


