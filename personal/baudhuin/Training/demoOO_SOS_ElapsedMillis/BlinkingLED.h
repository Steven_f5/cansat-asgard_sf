
#pragma once
#include "Arduino.h"
#include "elapsedMillis.h"

class BlinkingLED {
  public:
    BlinkingLED(byte pinNumber, unsigned long duration);
    void run();
  private:
    byte pinNumber;
    uint16_t duration;
    elapsedMillis elapsed;
} ;
