/*
    TC_Timer.h
*/

#pragma once
#include "Arduino.h"

/**
    @ingroup cansasgardCSPU
    @brief A class to encapsulate interactions with the TC timers of the SAMD21 processors
    When the timer expires the TC4_Handler() or TC5_Handler() interrupt handler is called.
    Anyone instanciating the TC_Timer has to provide the corresponding interrupt handler,
    with signature void TCx_Handler() where x = 3, 4 or 5. In the handler, it is mandatory to
    clear the interrupt to allow for the timer to run again (not clearing it would possibly 
    block the CPU. Optionnally, method isYourInterrupt() can be used to check whether the interruption
    was actually triggered by the timer (is some cases, several sources could trigger the same
    interruption).   The source timer is the master clock assumed to by 48MHz. 
    @warning This class is tested with Adafruit Feather M0 express only.

    @usage

    TcTimer myTimer(TC_Timer::HwTimer::timerTC4);

    void setup() {
      ...
      myTimer.configure(100); // set to 100 Hz (for instance)
      tc3.enable();           //start the timer
      ...
    }

    void TC3_Handler (void) {
      if (tc3.isYourInterrupt()) {
          ... // Your actual code here
          tc3.clearInterrupt(); // clear the interrupt so that it will run again
      }
    }
    @code
    @endcode
    
    This code is an generalisation and encapsulation of
    https://gist.github.com/jdneo/43be30d85080b175cb5aed3500d3f989
*/
class TC_Timer {
  public:
    /** The enum to select which hardware timer will be used */
    enum class HwTimer {
      timerTC3,
      timerTC4,
      timerTC5
    };
    /** Constructor. Be sure to create ONE instance only for a particular hardware timer
        @param hwTimer The hardware timer to use.
    */
    TC_Timer(HwTimer hwTimer = HwTimer::timerTC5);
    /** Configure the TC to generate output events at the sample frequency.
        @param frequencyHz The frequency of the the interrupt generated by the timer, in Hz.
    */
    void configure(uint16_t frequencyHz);

    /** Update the time frequency. This can be done at any moment after the timer was configured
        @param frequencyHz The frequency of the the interrupt generated by the timer, in Hz.
    */
    void setFrequency(uint16_t frequencyHz);

    /** Reset and re-enable the timer. */
    void reset();

    /** Disable the timer. Re-enable using reset followed by enable */
    void disable();
    /** Enable the timer and wait for it to be ready */
    void enable();

    /** Clear interrupt raised when the timer expires. Must be called in the interrupt handler, to allow the
        interrupt to be raised again.
    */
    void clearInterrupt();

    /** This function is for use in the interrupt handler, to make sure the timer is the origin of the interruption 
     *  @return True if the interrupt was generated by the timer, false otherwise.
     */
    bool isYourInterrupt();
    
  protected:
    /** Check if TC5 is done syncing
        @return true when still busy syncing
    */
    bool isSyncing();
    
  private:
    Tc*	tcTimer; // Pointer to the base address for the SAMD TC timer.
};
