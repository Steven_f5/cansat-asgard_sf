// Useless test: Ansi escape sequences are not supported by Arduino IDE nor CoolTerm

const char Esc[]="\x1B\x5b";

void cursorBack(byte n) {
  Serial.print(Esc);
  Serial.print(n);
  Serial.print('D');
}

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
while (!Serial);

Serial.print("Longueur de LineUp:");
Serial.println(strlen(Esc));
Serial.println("Ceci est une ligne de texte");
Serial.print("Ceci est une 2e ligne de texte[]");
delay(1000);

cursorBack(5);
delay(1000);
Serial.print("OVERWRITE");

}

void loop() {
  // put your main code here, to run repeatedly:

}
