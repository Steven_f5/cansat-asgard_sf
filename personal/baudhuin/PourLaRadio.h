/*
 * PourLaRadio.h
 *
 */

#pragma once

#include "Arduino.h"
#include "IsaTwoRecord.h"


// A METTRE DANS la lib CansatAsgard
class XBeeClient {
public:
	XBeeClient();
	/** Initialize point to point radio link
	 *  @param stream The serial object to use to communicate with
	 *  			  the XBee module.
	 *  AJOUTER TOUS LES PARAMETRES NECESSAIRES
	 *  @return true if success, false if any problem encountered */
	bool begin(HardwareSerial &stream);
	/** Send data.
	 *  @param data The payload to transfer
	 *  @param dataSize The number of bytes of the payload
	 *  @return True if transfer successful, false otherwise.
	 */
	bool send(void* data, byte dataSize);
	/** Check whether data is received
	 *  @param payload If anything is received, this pointer is set
	 *  			   to an internal buffer containing the received
	 *  			   payload.
	 *  @param payloadSize If anything is received, this parameters is
	 *  			   set to the number of bytes of the received payload.
	 *  @return True if anything received, false otherwise
	 */
	bool receive(void* &payload, byte& payloadSize);

};

// Dans la lib IsaTwoCSPU
class IsaTwoXBeeClient : public XBeeClient {
	bool send(const IsaTwoRecord& record);
};
