/*
   This is a test program to test the SI4432 capabilities, using the break-out from ElectroDragon
   (https://www.electrodragon.com/w/Si4432).
   It reports the power detected when tuned to frequencies in range min-max, using a constant step,
   using the default modem configuration (FSK, 2kbps).

   It uses the RadioHead Packet Radio library which supercedes the RH_22 library.
   RadioHead documentation: http://www.airspayce.com/mikem/arduino/RadioHead/
   SI4432 datasheet in folder 2110
   EZRadioPRO Detailed Register Descriptions in folder 2110

   Belgian frequency allocation: https://www.bipt.be/en/operators/radio/frequency-management/frequency-plan
       Public mobile networks are in 791-821 Mhz (= 800Mhz band) and 880-915Mhz  (=900MHz band) range
       Belgian GSM Networks: http://tibius.be/2014/06/09/levolution-reseaux-mobiles-3g-en-900-mhz/ 
          2G network: 900 MHz
          3G network: 2100MHz + reuse of 900Mhz
          4G network: 1800MHz + partly 800MHz

   PERFORMANCE: 
       Unmodulated carrier is detected exactly at its frequency and (with a lower RSSI) at frequency+- 0.1MHz
       Detected RSSI significantly increases in range [F-0.5 MHz ; F+2 MHz]
       
   OPEN ISSUES:
     - Do we need to entre Rx mode after each frequency change ? Done for safety.
     - CONNECT INTERRUPT PIN (No change, is it required just for the RSSI ??!
     - Emitter sometimes succeed in initialising the RF22, but it takes up to 13 minutes! Definetely
       something wrong....  This was a level issue: it does not happen with Feather M0 Express (after fixing a
       lose connexion) 
       Should we connect the interrupt pin (constructor defaults to interruptPin=2, we use 5 since
       Feather do not have a 2 pin). Is it required when we do not plan to actually receive data 
       (and possibly don't plan to send any) ?
     - Can we detect PSK or AM activity with a FSK modem ? If detection is only about detecting
       a carrier wave, modulation should be irrelevant...TO BE TESTED: any change in detection
       if only the modulation changes ? 
     - What kind of modulations are actually used in our frequency range ?
     - Should we also scan using OOK (On-off Keying) modem config (=AM).
     

     FIXED ISSUES:
     - First cycle always reports maximum RSSI while next ones report 0. Was a level-shifting issue. 
     - Is the carrier wave emitted even when no data is transmitted ? YES. We detect unmodulated carrier
       sent by the Emitter sketch. 
     - Do we need to connect the various grounds of the SS4432 together? No, they are internally connected. 

        
    Operation modes.
     The transceiver supports 3 modes: RX, TX and idle. Toggle using setModeRx(), setModeTx() and
     setModeIdle().
         * The scanner sets the Rx mode in setup() and never leaves it.
         * The emitter sets the Idle mode in setup() and switches to Tx mode to emit the carrier.
         
   Connexions: 
   - SS4432 to Feather
      VCC - 3V (!! 5V not supported, max 3.6V)
      GND - GND
      SDO - MISO
      SDI - MOSI
      SCLK- SCK
      nSEL- 10
      SDN - GND
      nIRQ- 2
  - SS4432 to Genuino Uno (Use a level shifter!)
      VCC - 3V (!! 5V not supported, max 3.6V)
      GND - GND
      SDO - MISO=12
      SDI - MOSI=11
      SCLK- SCK=13
      nSEL- 10 
      SDN - GND 
      nIRQ - 2  
*/

#define DEBUG     // To activate debugging functions in DebugCSPU.h
#include "DebugCSPU.h"
//#include "SPI.h"   // THIS IS NOT REQUIRED (confirmed).
#include "RH_RF22.h"
#include "elapsedMillis.h"

#define FREQ_ALL                    0
#define FREQ_PUBLIC_MOBILE_NETWORK  1
#define FREQ_433_500                2
#define FREQ_433_440                3
// ------------------------------------------
// Update the next constants to define behaviour 
#define FREQUENCY_RANGE FREQ_ALL   // Update to select scanning frequency settings
const float scanFrequencyStep  = 10;  // MHz. The frequency step used to scan.
const uint8_t minRSSI_ToReport=20;  // Only RSSI values >= minRSSI_ToReport are reported in the output.

// ------------------------------------------
// The following constants should not require tuning once the program is operational
#define DBG_SCANNING_STEP 0  // Define to 1 for detailed debugging output, 0 for regular output. 


#if (FREQUENCY_RANGE==FREQ_ALL) 
const float minFrequency = 433.0; // MHz 433=minimum supported by SI4432, min supported by driver=240.0
const float maxFrequency = 868.0; // MHz  868=maximum supported by SI4432, max supported by driver=960.0
#elif (FREQUENCY_RANGE==FREQ_PUBLIC_MOBILE_NETWORK)
  const float minFrequency = 791.0; // MHz 
  const float maxFrequency = 821.0; // MHz 
#elif (FREQUENCY_RANGE==FREQ_433_500)
  const float minFrequency = 433.0; // MHz 
  const float maxFrequency = 500.0; // MHz 
#elif (FREQUENCY_RANGE==FREQ_433_440)
  const float minFrequency = 433.0; // MHz 
  const float maxFrequency = 440.0; // MHz 
#else#else 
#  error "Invalide FREQUENCY_RANGE value:" FREQUENCY_RANGE
#endif

const byte CS_Pin=10;                   // Digital pin used as Chip-Select line for the SS4432
const byte InterruptPin=2;              // Digital pin used by the SS4432 to trigger interruption
const uint8_t numRSSI_Samples  = 10;    // The number of RSSI samples taken for each frequency.
const byte maxInitRetries=100;          // The maximum number of retries for the RF22 init. 
const bool showStatusRegister=false;    // If true, status register is printed regularly.

// Singleton instance of the radio
RH_RF22 rf22(CS_Pin,InterruptPin);
float currentFrequency;
elapsedMillis elapsedSinceFrequencyChange;
elapsedMillis cycleDuration;
uint8_t minRSSI=255, maxRSSI=0;

void printStatusRegister() {
  if (!showStatusRegister) return;
  uint8_t reg=rf22.statusRead();
  Serial << "Status register: " << reg << ":";
  Serial.print(reg & 0b10000000 ? "RxTx FIFO overflow " : " ");
  Serial.print(reg & 0b01000000 ? "RxTx FIFO underflow " : " ");
  Serial.print(reg & 0b00100000 ? "RxTx FIFO empty " : "RxTx FIFO not empty ");
  Serial.print(reg & 0b00010000 ? "Header error " : " ");
  Serial.print(reg & 0b00001000 ? "Frequency error " : " ");
  Serial.print(reg & 0b00000100 ? "?? " : " ");
  switch(reg & 0b00000011) {
    case 0b00:
        Serial << "IDLE";
        break;
    case 0b01: 
        Serial << "RX-State";
        break;
    case 0b10:
        Serial << "TX-State";
        break;
    default:
        Serial << "**Unexpected state**";
  }
  Serial << ENDL;
}

void setup() {
  DINIT(115200);
  Serial << "****************************" << ENDL;
  Serial << "**   RF Scanner utility   **" << ENDL;
  Serial << "****************************" << ENDL;

  // Next two line are unnecessary (confirmed).
  // pinMode(CS_Pin, OUTPUT);
  // SPI.begin();
  // Serial << "SPI Init OK" << ENDL; 

  Serial << "Waiting for RF22 initialization...";
  Serial.flush();
 // delay(3000); // UNNECESSARY ! wait before using the RF22: first attempt always fails. Waiting 2 sec is not alwyas enough
 // Serial.println();
  byte count=0;
  while (!rf22.init()) {
    Serial << "RF22 init failed.";
    if  (count < maxInitRetries) {
        count++;
        Serial << " Retrying (" << count << "/" << maxInitRetries << ")..." << ENDL;
        delay(500);
    } else {
      Serial << "RF22 initialization failed (Aborted)" << ENDL;
      Serial.flush();
      exit(-1);
    }   
  }
  Serial << "RF22 init OK" << ENDL; 
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36 (2.4 kbps)
  currentFrequency = minFrequency;
  if (!rf22.setFrequency(currentFrequency)) {
    Serial << "Error setting frequency to " << currentFrequency << " MHz" << ENDL;
    Serial.flush();
    exit(-1);
  }
  elapsedSinceFrequencyChange=0;
  rf22.setModeRx();
  Serial << "Setup OK." << ENDL;
  Serial << "Scanning from " << minFrequency << " to " << maxFrequency
         << " MHz (step = " << scanFrequencyStep  << " Mhz)" << ENDL;
  Serial << "Only RSSI >= " << minRSSI_ToReport << " internal units is reported." << ENDL;
  printStatusRegister();
  delay(2000);  // Without this delay, the first cycle report readings while next ones report 0. 
  printStatusRegister();
  cycleDuration=0;  // Reset cycle duration as last step in setup(). 
}

/* 
 * From the SI4432 Datasheet, §8.10:
 *  The RSSI value may be incorrect if read during the update period. The update period is approximately 10 ns every
    4 Tb. For 10 kbps, this would result in a 1 in 40,000 probability that the RSSI may be read incorrectly. This
    probability is extremely low, but to avoid this, one of the following options is recommended: majority polling,
    reading the RSSI value within 1 Tb of the RSSI interrupt, or using the RSSI threshold described in the next
    paragraph for Clear Channel Assessment (CCA).

   NB: 1 in 40000 probability means that update duration/update period = 1 /40000 ie update period = 40000*10 ns = 400µs. 
       Tb = duration of 1 bit = 1/10 kb = 0,1 ms. 4Tb = 400µs, OK. 
   Baud rate is defined by configuring the modem with setModemConfig. Min. is 2kps which results in an update period of 2 msec.
   Hence, whatever modem configuration is ok for scanning 1 frequency in less than 0,5 msec. with the following timing:
      set frequency
      minimum delay of 300 µs (synthetizer settling time is 200µs according to datasheet). 
      3 samples with the delay of the SPI communication which is more than 40 ns. 
   To avoid the use of micros() which overflows every 70 min and could consequently block the scanning for 70 minutes,
   we use a 1 ms. delay after the frequency change (without active waiting). 
   Complete scan from 433 to 868 MHz with 10 MHz step would be 45 steps, i.e. less than 100 ms. 

   @return True if the maximum frequency has been reached (and the scanning cycle completed), false otherwise. 
 */
bool performScanningStep() {
  if (elapsedSinceFrequencyChange < 1) {
    // Noop if frequency is not settled. 
    return false;
  }
  bool result=false;
  uint16_t rssi=0;
  printStatusRegister();
  for (int i=0; i < numRSSI_Samples; i++) {
    rssi+=rf22.rssiRead();
    delay(1);
    DPRINTS(DBG_SCANNING_STEP, "  cumulated rssi=");
    DPRINTLN(DBG_SCANNING_STEP, rssi);
  }
  rssi/=numRSSI_Samples;
  if (rssi > maxRSSI) maxRSSI=rssi;
  if (rssi < minRSSI) minRSSI=rssi;

  float power=0;
  // Conversion to dBm 
  // TO BE COMPLETED BASED ON Fig. 31 in datasheet. 
  // NB: 1 dBm:    Power = x dBm <=> x = 10 log (P/1 mW), P = 1 mW . 10^(x/10)

  float correctedPower=0;
  // Compensate for Antenna gain
  // TO BE COMPLETED. Could be performed on the ground? 

  if ( rssi >= minRSSI_ToReport) {
    Serial << "RSSI at " << currentFrequency << " MHz = " << rssi 
           << " (internal units, from register RH_RF22_REG_26_RSSI)= " 
           << power << " dBM (corrected: " 
           << correctedPower << ")" << ENDL;
  }
  
  currentFrequency += scanFrequencyStep;
  if (currentFrequency > maxFrequency) {
    currentFrequency = minFrequency;
    result = true;
  }
  if (!rf22.setFrequency(currentFrequency)) {
    Serial << " **** Error setting frequency to " << currentFrequency << " MHz ****" << ENDL;
  }
  rf22.setModeRx(); // required ? Maybe the setFrequency() switches back to idle? 
  elapsedSinceFrequencyChange=0;
  DPRINTS(DBG_SCANNING_STEP, "Frequency set to ");
  DPRINT(DBG_SCANNING_STEP, currentFrequency);
  DPRINTSLN(DBG_SCANNING_STEP, " MHz");
  return result;
}

void loop() {
  
  // Run a scan calling performScanningStep() until it returns true, and what 2 secs before launching
  // the next one. 
  
  if (performScanningStep()) {
    Serial << "--------- Scanning cycle complete in " << cycleDuration 
           << " msec. RSSI in range [" << minRSSI <<" MHz ; " << maxRSSI << " MHz] ---------" << ENDL << ENDL;
    minRSSI=255; 
    maxRSSI=0;
    delay(2000);
    cycleDuration = 0;
  }
}
