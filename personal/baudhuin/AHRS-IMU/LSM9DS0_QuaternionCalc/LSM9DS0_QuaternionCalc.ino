/*
   This sketch is a test, based on the ahrs_mahony example from
   Adafruit_AHRS library (which strangely enough is not limited to mahony algorithm....).
*/

//#define USE_MAHONY // Comment out to use Madgwick filter) 
//#define APPLY_MAG_CALIBRATION 

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM9DS0.h>

#ifdef USE_MAHONY
#include <MahonyAHRS.h>
#else
#include <MadgwickAHRS.h>
#endif

// Note: This sketch requires the MahonyAHRS sketch from
// https://github.com/PaulStoffregen/MahonyAHRS, or the
// MagdwickAHRS sketch from https://github.com/PaulStoffregen/MadgwickAHRS

// Note: This sketch is a WORK IN PROGRESS

// Create sensor instances.
Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(1000);  // Use I2C, ID #1000
Adafruit_LSM9DS0::Sensor accel, mag, gyro;

#ifdef OLDOLD
Adafruit_L3GD20_Unified       gyro(20);
Adafruit_LSM303_Accel_Unified accel(30301);
Adafruit_LSM303_Mag_Unified   mag(30302);
#endif

// Mag calibration values are calculated via ahrs_calibration.
// These values must be determined for each baord/environment.
// See the image in this sketch folder for the values used
// below.

#ifdef APPLY_MAG_CALIBRATION
// Offsets applied to raw x/y/z values
float mag_offsets[3]            = { -91.868F, -71.419F, 55.988F };

// Soft iron error compensation matrix
float mag_softiron_matrix[3][3] = { { 1.06f,  -0.013f,  -0.094f },
                                    { -0.028f, 1.14,    -0.011f },
                                    { -0.035,  -0.066f, 1.173f }
};

float mag_field_strength        = 59.31F;
#endif

// Mahony is lighter weight as a filter and should be used
// on slower systems
#ifdef USE_MAHONY
Mahony filter;
#else
Madgwick filter;
#endif

void configureSensor(void)
{
  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

  // 2.) Set the magnetometer sensitivity
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
}

void setup()
{
  Serial.begin(115200);

  // Wait for the Serial Monitor to open (comment out to run without Serial Monitor)
  while(!Serial);

  Serial.println(F("Adafruit 9-10 DOF Board AHRS Calibration Example"));
#ifdef USE_MAHONY
  Serial.print(F("   using Mahony filter"));
#else
  Serial.print(F("   using Madgwick filter"));
#endif
#ifdef APPLY_MAG_CALIBRATION
  Serial.print(F(", with magnetic calibration"));
#else
  Serial.print(F(", no magnetic calibration"));
#endif
  Serial.println("");

  if (!lsm.begin())
  {
    /* There was a problem detecting the LSM9DS0 ... check your connections */
    Serial.print(F("Ooops, no LSM9DS0 detected ... Check your wiring or I2C ADDR!"));
    while (1);
  }
  Serial.println(F("Found LSM9DS0 9DOF"));
  configureSensor();
  Serial.println(F("Sensor configured"));

  accel = lsm.getAccel();
  mag = lsm.getMag();
  gyro= lsm.getGyro();
  // Filter expects 50 samples per second
  filter.begin(50);
  Serial.println(F("filter init ok"));

}

void loop(void)
{
  sensors_event_t gyro_event;
  sensors_event_t accel_event;
  sensors_event_t mag_event;
  sensors_event_t temp_event;
  
  // Get new data samples
  lsm.getEvent(&accel_event, &mag_event, &gyro_event, &temp_event);
#ifdef OLDOLD
  gyro.getEvent(&gyro_event);
  accel.getEvent(&accel_event);
  mag.getEvent(&mag_event);
#endif

#ifdef APPLY_MAG_CALIBRATION
  // Apply mag offset compensation (base values in uTesla)
  float x = mag_event.magnetic.x - mag_offsets[0];
  float y = mag_event.magnetic.y - mag_offsets[1];
  float z = mag_event.magnetic.z - mag_offsets[2];

  // Apply mag soft iron error compensation
  float mx = x * mag_softiron_matrix[0][0] + y * mag_softiron_matrix[0][1] + z * mag_softiron_matrix[0][2];
  float my = x * mag_softiron_matrix[1][0] + y * mag_softiron_matrix[1][1] + z * mag_softiron_matrix[1][2];
  float mz = x * mag_softiron_matrix[2][0] + y * mag_softiron_matrix[2][1] + z * mag_softiron_matrix[2][2];
#else
  float mx = mag_event.magnetic.x;
  float my = mag_event.magnetic.y;
  float mz = mag_event.magnetic.z; 
#endif

  // The filter library expects gyro data in degrees/s, but adafruit sensor
  // uses rad/s so we need to convert them first (or adapt the filter lib
  // where they are being converted)
  float gx = gyro_event.gyro.x * 57.2958F;
  float gy = gyro_event.gyro.y * 57.2958F;
  float gz = gyro_event.gyro.z * 57.2958F;



  // Update the filter
  filter.update(gx, gy, gz,
                accel_event.acceleration.x, accel_event.acceleration.y, accel_event.acceleration.z,
                mx, my, mz);

  // Print the orientation filter output
  float roll = filter.getRoll();
  float pitch = filter.getPitch();
  float heading = filter.getYaw();
  Serial.print(millis());
  Serial.print(" - Orientation: ");
  Serial.print(heading);
  Serial.print(" ");
  Serial.print(pitch);
  Serial.print(" ");
  Serial.println(roll);

  delay(10);
}
