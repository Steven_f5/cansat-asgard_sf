/*
   SD Logger.cpp
*/

#define USE_ASSERTIONS
//#define DEBUG
#include "DebugCSPU.h"

#define DBG_INIT_SD 0
#define DBG_LOG_SD 0
#define DBG_BUILD_FILE_NAME_SD 0
#define DBG_FILESIZE 0
#define DBG_FREE_SPACE 0
#define DBG_IDLE 1

#include "SD_Logger.h"

SD_Logger::SD_Logger() {
  logFileName = "NoName.txt";
  timeSinceMaintenance = 0;
}

char SD_Logger::init( const char * fourCharPrefix,
                      const byte chipSelectPinNumber,
                      const String& msg,
                      const unsigned int requiredFreeMegs) {

  DPRINTS(DBG_INIT_SD, "Initializing SD card...");
  if (!SD.begin(chipSelectPinNumber)) {
    DPRINTSLN(DBG_INIT_SD, "Card failed, or not present");
    return -1;
    // do nothing more
  }
  DPRINTSLN(DBG_INIT_SD, "SD.begin(): OK ");

  if (requiredFreeMegs > freeSpaceInMBytes() ) {
    DPRINTS(DBG_INIT_SD, "Not enough space");
    return 1;
  };

  // Let's define the filename to use.
  unsigned int counter = 0;
  do {
    counter++;
    buildFileName(fourCharPrefix, counter);
  } while ( SD.exists(fileName().c_str()));
  DPRINTS(DBG_INIT_SD, "Name of the file: ");
  DPRINTLN(DBG_INIT_SD, fileName());

  // Let's open/create the file (to check the card is not read-only)
#ifdef CLOSE_AFTER_EACH_ACCESS
  File logFile;
#endif 
  logFile = SD.open(fileName(), FILE_WRITE);
  if (!logFile) {
    DPRINTS(DBG_INIT_SD, "Card read-only?");
    return -1;
  }

  // Finally, let's  write to it, if msg is not empty
  if (msg.length() > 0) {
    logFile.println(msg);
    DPRINTS(DBG_INIT_SD, "Wrote ");
    DPRINTLN(DBG_INIT_SD, msg);
  }
#ifdef CLOSE_AFTER_EACH_ACCESS
  logFile.close();
#else
  logFile.flush();
  DASSERT(logFile); 
#endif
  DPRINTSLN(DBG_INIT_SD, "SD_Logger ready!");
  return 0;
}


bool SD_Logger::log(const String &data) {
#ifdef CLOSE_AFTER_EACH_ACCESS
  File logFile = SD.open(fileName(), FILE_WRITE);
#else
  DASSERT(logFile); 
#endif
  bool result = false;
  if (logFile) {
    logFile.println(data);
#ifdef CLOSE_AFTER_EACH_ACCESS
    logFile.close(); // Strangely enough, performance is *better* when closing file after each write.
#else
    logFile.flush();
#endif
    DPRINTSLN(DBG_LOG_SD, "data written");
  }
  else {
    DPRINTS(DBG_LOG_SD, "error opening file ");
  }
  return result;
}

const String& SD_Logger::fileName() const {
  return logFileName;
}

bool SD_Logger::doIdle() {
  bool result=false;
  if (timeSinceMaintenance > maintenancePeriod) {
#ifndef CLOSE_AFTER_EACH_ACCESS
    logFile.close();
    logFile = SD.open(fileName(), FILE_WRITE);
    if (!logFile) {
      DPRINTSLN(DBG_IDLE, "Error reopening file!");
    }
#endif
    DPRINTSLN(DBG_IDLE, "Maintenance performed");
    result=true;
    timeSinceMaintenance = 0;
  }
  return result;
}

void SD_Logger::buildFileName(const char* fourCharPrefix, const byte number)  {

  logFileName = fourCharPrefix;
  DASSERT(logFileName.length() == 4);

  char numStr[5];
  sprintf(numStr, "%04d", number);

  logFileName += numStr;
  logFileName += ".txt";

  DPRINTS(DBG_BUILD_FILE_NAME_SD, "File name: ");
  DPRINTLN(DBG_BUILD_FILE_NAME_SD, logFileName);
}

unsigned long SD_Logger::fileSize() {
  unsigned long size = 0;
#ifdef CLOSE_AFTER_EACH_ACCESS
  File logFile = SD.open(fileName(), FILE_READ);
  DPRINTSLN(DBG_FILESIZE, "Reopened file ");
#else
  DASSERT(logFile);
#endif
  if (logFile) {
    size = logFile.fileSize();
    DPRINTS(DBG_FILESIZE, "FileSize()= ");
    DPRINTLN(DBG_FILESIZE, logFile.fileSize());
#ifdef CLOSE_AFTER_EACH_ACCESS
    logFile.close();
#endif
  }
  else {
    DPRINTS(DBG_FILESIZE, "error opening file ");
  }
  return size;
}

unsigned long SD_Logger::freeSpaceInBytes()  {
  const unsigned long overFlowLimit = UINT_MAX / 512;
  unsigned long  freeBytes = SD.vol()->freeClusterCount();
  freeBytes *= SD.vol()->blocksPerCluster();
  if (freeBytes >= overFlowLimit) {
    freeBytes = UINT_MAX;
  } else freeBytes *= 512;
  DPRINTS(DBG_FREE_SPACE, "Free space bytes: ");
  DPRINTLN(DBG_FREE_SPACE, freeBytes);
  return freeBytes;
}

unsigned long SD_Logger::freeSpaceInMBytes()
{
  unsigned long  freeMB = SD.vol()->freeClusterCount();
  freeMB *= SD.vol()->blocksPerCluster();
  freeMB /= 2048;
  DPRINTS(DBG_FREE_SPACE, "Free space MB: ");
  DPRINTLN(DBG_FREE_SPACE, freeMB);
  return freeMB;
}


