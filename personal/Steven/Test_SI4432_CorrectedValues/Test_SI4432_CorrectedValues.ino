#include "SI4432_Client.h"

SI4432_Client si4432(5, 6);
float minFreqMHz = 433.0;
float maxFreqMHz = 868.0;
float stepFreqMHz = 5.0;
unsigned int nbData = (maxFreqMHz - minFreqMHz) / stepFreqMHz + 1;
powerDBm_t *power = new powerDBm_t[nbData];

void setup()
{
    si4432.begin();
    delay(2000);
    si4432.readPower(power, nbData, 1, minFreqMHz, maxFreqMHz, stepFreqMHz);
    for (unsigned int i = 0; i < nbData; i++)
    {
        Serial << minFreqMHz + i * stepFreqMHz << ", " << power[i] << ", " << ENDL;
    }
    Serial << ENDL << ENDL;
}

void loop() {}
