#include "SI4432_Client.h"

SI4432_Client si4432(5, 10);
float readings[10];
void setup() {
  // put your setup code here, to run once:
  si4432.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("1 read : ");
  Serial.println(si4432.readPower(500));
  si4432.readPower(&readings[0], 10);
  Serial.println("10 reads");
  for(unsigned int i = 0; i< 10; i++){
    Serial.println(readings[i]);
  }
  Serial.println();
  
}
