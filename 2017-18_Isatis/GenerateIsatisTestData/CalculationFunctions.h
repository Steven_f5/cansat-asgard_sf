unsigned long addTimeJitter(unsigned long t) ;

float addAltitudeJitter(float alt);

unsigned long getMeasurementDuration(unsigned long t) ;

float getPressureFromAltitude(float altitudeFromGround);

float getTemperatureFromAltitude(float altitudeFromGround);

float getGPY_OutputV(float relativeAltitude);

bool getGPY_Quality();
