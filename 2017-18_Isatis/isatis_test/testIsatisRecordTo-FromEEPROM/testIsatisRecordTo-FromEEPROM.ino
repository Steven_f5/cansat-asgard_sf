
#include "IsatisHW_Scanner.h"
#include "EEPROM_Bank.h"
#include "IsatisConfig.h"
#include "IsatisDataRecord.h"


const unsigned long NUM_RECORDS=20;

IsatisHW_Scanner hw;
EEPROM_Bank eb;

void hexDump(IsatisDataRecord &rec)
{
  byte *data = (byte*) &rec;
  byte size = sizeof(IsatisDataRecord);
  char str[10];
  for (int i = 0; i < size; i++) {
    if ((i % 16) == 0) {
      if (i != 0) Serial << ENDL;
      sprintf(str, "  %04x:", i);
      Serial << str;
    }
    if ((i % 4) == 0) Serial << "  ";
    sprintf(str, "%02x ", data[i]);
    Serial << str;
  }
  Serial << ENDL;
}

void setup() {
  Serial.begin (19200);

  hw.IsatisInit();
  hw.printFullDiagnostic(Serial);

  IsatisDataRecord reference;
  IsatisDataRecord buffer;
  unsigned long dataSize = sizeof(IsatisDataRecord);

  reference.startTimestamp = 123456;
  reference.endTimestamp = 987654;
  reference.BMP_Temperature = 21.5 ;
  reference.BMP_Pressure =  1011.234;
  reference.BMP_Altitude = 51.098 ;
  reference.GPY_OutputV = 0.45678 ;
  reference.GPY_Quality = true ;
#ifndef USE_MINIMUM_DATARECORD
  reference.GPY_Voc = 0.321 ;
  reference.GPY_DustDensity = 0.1234 ;
  reference.GPY_AQI = 98.76 ;
#endif

  bool result = eb.init(EEPROM_KeyValue, hw, dataSize);
  if (!result) {
    Serial << "Resetting EEPROM bank content" << ENDL;
    result = eb.init(EEPROM_KeyValue, hw, dataSize);
    if (!result) {
      Serial.println("!!ERROR during init of EEPROM_Bank");
      Serial.flush();
      exit(0);
    }
  }
  eb.erase();

  unsigned long numLeft = eb.recordsLeftToRead();
  unsigned long numFree = eb.getNumFreeRecords();

  Serial.println();
  Serial.print("Header key             : 0x");
  Serial.println(EEPROM_KeyValue, HEX);

  Serial.print("Size of Isatis record  : ");
  Serial.print(sizeof(buffer));
  Serial.println(" bytes.");

  Serial.print("Used space             : ");
  Serial.print(numLeft);
  Serial.print(" records = ");
  Serial.print((numLeft / 1000.0)*IsatisAcquisitionPeriod / 60.0);
  Serial.println(" min. of operation.");
  Serial.print("Free space             : ");
  Serial.print(numFree);
  Serial.print(" records = ");
  Serial.print((numFree / 1000.0)*IsatisAcquisitionPeriod / 60.0);
  Serial.println(" min. of operation.");

  Serial.println("----------");
  Serial << ENDL << "Writing "<< NUM_RECORDS << " record(s)" << ENDL;
  
  buffer.printCSV_Header(Serial);
  Serial << ENDL;
  reference.printCSV(Serial);
  Serial << ENDL; 
  hexDump(reference);
  for (int i = 0; i< NUM_RECORDS ; i++) {
    eb.storeOneRecord((byte*) &reference, dataSize);
  }
  eb.doIdle(true);


  Serial << ENDL << "Reading record(s) back..." << ENDL;
  eb.resetReader();
  numLeft = eb.recordsLeftToRead();
  Serial<<F("EEPROM contains ") << numLeft << " records)" << ENDL;
  for (int i = 0; i < NUM_RECORDS; i ++ ) {
    eb.readOneRecord((byte*)&buffer, sizeof(IsatisDataRecord));
    buffer.printCSV(Serial);
    Serial<<ENDL;
    hexDump(buffer);

    // Checking
    bool error = false;
    if ( reference.BMP_Temperature != buffer.BMP_Temperature ) error = true;
    if ( reference.BMP_Pressure != buffer.BMP_Pressure ) error = true;
    if ( reference.BMP_Altitude != buffer.BMP_Altitude ) error = true;
    if ( reference.GPY_OutputV != buffer.GPY_OutputV ) error = true;
    if ( reference.GPY_Quality != buffer.GPY_Quality ) error = true;
#ifndef USE_MINIMUM_DATARECORD
    if ( reference.GPY_Voc != buffer.GPY_Voc ) error = true;
    if ( reference.GPY_DustDensity != buffer.GPY_DustDensity ) error = true;
    if ( reference.GPY_AQI != buffer.GPY_AQI ) error = true;
#endif
    if ( reference.startTimestamp != buffer.startTimestamp ) error = true;
    if ( reference.endTimestamp != buffer.endTimestamp ) error = true;
    if (error) {
      Serial << "  *** ERROR: erroneous value detected **** " << ENDL;
    }
  } // for
  eb.resetReader();
  numLeft = eb.recordsLeftToRead();
  Serial<<F("End of job (EEPROM contains ") << numLeft << " identicial records)" << ENDL;
}

void loop() {
  // put your main code here, to run repeatedly:

}
